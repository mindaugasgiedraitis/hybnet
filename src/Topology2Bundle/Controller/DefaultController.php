<?php

namespace Topology2Bundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Graphp\GraphViz\GraphViz;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class DefaultController
 * @package Topology2Bundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function topologyDiscoverAjaxAction(Request $request)
    {
        try {
            $topology = $this->get('hybnet.topology_service_2')->discoverTopology();
            $response = new Response(json_encode($topology->getArrayForDisplay()));
        } catch (\Exception $e) {
            $response = new Response($e->getMessage());
        }
        return $response;
    }

    public function testAction(Request $request)
    {
        $graphviz = new GraphViz();
        $topology = $this->get('hybnet.topology_service_2')->discoverTopology();
        $result = $graphviz->createImageHtml($topology);
        
        return $this->render('Topology2Bundle:Default:index.html.twig', array('img' => $result));
    }
}
