<?php

namespace Topology2Bundle\Object;

use Fhaculty\Graph;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class Topology
 * @package TopologyBundle\Object
 */
class Topology extends Graph\Graph
{
    /**
     * @param int|string $id
     * @return NodeConnector|null
     */
    public function getNodeConnector($id)
    {
        $result = $this->vertices->getVertexId($id);
        if ($result instanceof NodeConnector) {
            return $result;
        }
        return null;
    }

    /**
     * @param int|string $id
     * @return Node|null
     */
    public function getNode($id)
    {
        $result = $this->vertices->getVertexId($id);
        if ($result instanceof Node) {
            return $result;
        }
        return null;
    }

    /**
     * {id: 1, label: 'Node 1'},
     * {id: 2, label: 'Node 2'},
     * {id: 3, label: 'Node 3'},
     * {id: 4, label: 'Node 4'},
     * {id: 5, label: 'Node 5'}
     *
     * {from: 1, to: 3},
     * {from: 1, to: 2},
     * {from: 2, to: 4},
     * {from: 2, to: 5}
     */

    /**
     * @return array
     */
    public function getArrayForDisplay()
    {
        $vertices = $this->getVertices();

        $nodeIds = array();
        $linkIds = array();

        /** @var Node $node */
        foreach ($vertices as $node) {
            $nodeIds[] = array(
                'id' => $node->getId(),
                'label' => $node instanceof NodeConnector ? $node->getInterfaceName() : $node->getId(),
                'shape' => $node instanceof Node ? 'ellipsis' : 'text',
            );

            $edges = $node->getEdges();
            /** @var Graph\Edge\Undirected $link */
            foreach ($edges as $link) {
                $linkIds[] = array(
                    'from' => $node->getId(),
                    'to' => $link->getVertexFromTo($node)->getId(),
                );
                $link->destroy();
            }
        }

        return array(
            'nodes' => $nodeIds,
            'edges' => $linkIds,
        );
    }
}
