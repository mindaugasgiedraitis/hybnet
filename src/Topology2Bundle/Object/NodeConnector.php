<?php

namespace Topology2Bundle\Object;

use Fhaculty\Graph\Vertex;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class NodeConnector
 * @package TopologyBundle\Object
 */
class NodeConnector extends Vertex
{
    /**
     * @var String $mac
     */
    protected $mac;

    /**
     * @var String $interfaceName
     */
    protected $interfaceName;

    /**
     * @return String
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * @param String $mac
     * @return NodeConnector
     */
    public function setMac($mac)
    {
        $this->mac = $mac;
        return $this;
    }

    /**
     * @return String
     */
    public function getInterfaceName()
    {
        return $this->interfaceName;
    }

    /**
     * @param String $interfaceName
     * @return NodeConnector
     */
    public function setInterfaceName($interfaceName)
    {
        $this->interfaceName = $interfaceName;
        return $this;
    }

    /**
     * @param NodeConnector $nodeConnector
     * @return \Fhaculty\Graph\Edge\Undirected
     */
    public function connectToAnother(NodeConnector $nodeConnector)
    {
        return $this->createEdge($nodeConnector);
    }
}