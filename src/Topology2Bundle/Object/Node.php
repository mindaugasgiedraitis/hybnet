<?php

namespace Topology2Bundle\Object;

use Fhaculty\Graph\Vertex;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class Node
 * @package TopologyBundle\Object
 */
class Node extends Vertex
{
    const TYPE_OPENFLOW = 0;
    const TYPE_LEGACY = 1;

    /**
     * @var String $name
     */
    protected $name;

    /**
     * @var String $type
     */
    protected $type;

    /**
     * @var String $ipAddress
     */
    protected $ipAddress;

    /**
     * @return String
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param String $name
     * @return Node
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return String
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param String $type
     * @return Node
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return String
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param String $ipAddress
     * @return Node
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
        return $this;
    }

    /**
     * @param NodeConnector $nodeConnector
     * @return \Fhaculty\Graph\Edge\Undirected
     */
    public function addNodeConnector(NodeConnector $nodeConnector)
    {
        return $this->createEdge($nodeConnector);
    }
}