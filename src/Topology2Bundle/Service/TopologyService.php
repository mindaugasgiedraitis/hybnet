<?php

namespace Topology2Bundle\Service;

use Topology2Bundle\Object;
use CoreBundle\Service\AbstractService;
use DatabaseBundle\Entity;
use RestBundle\Object\RestCall;
use SNMPBundle\Object\SnmpRequest;
use Fhaculty\Graph;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class TopologyService
 * @package TopologyBundle2\Service
 */
class TopologyService extends AbstractService
{
    const OPENFLOW_TOPOLOGY_DISCOVER_URL = '/restconf/operational/network-topology:network-topology';
    const OPENFLOW_NODE_INFORMATION_URL = '/restconf/operational/opendaylight-inventory:nodes/node';

    const SNMP_LLDP_REM_PORT_ID_OID = '1.0.8802.1.1.2.1.4.1.1.7';
    const SNMP_LLDP_REM_MAC_OID = '1.0.8802.1.1.2.1.4.1.1.5';

    const SNMP_LLDP_LOC_PORT_ID_OID = '1.0.8802.1.1.2.1.3.7.1.3';
    const SNMP_LLDP_LOC_PORT_MAC_OID = '1.0.8802.1.1.2.1.3.2';

    const SNMP_LLDP_LOC_NAME_OID = '1.0.8802.1.1.2.1.3.3';

    const VLAN_HEX = '8D4'; // 2260 vlan

    /**
     * @return Object\Topology
     * @throws \Exception
     */
    public function discoverTopology()
    {
        $topology = new Object\Topology();

        $topologyInformation = $this->requestOpenFlowTopologyInformation();
        if ($topologyInformation) {
            $this->addOpenFlowTopology($topology, json_decode($topologyInformation, true));
        } else {
            throw new \Exception('OpenFlow topology missing.');
        }
        $this->addLegacyTopology($topology);

        return $topology;
    }

    /**
     * @return bool|string
     */
    public function requestOpenFlowTopologyInformation()
    {
        $restService = $this->getServiceContainer()->get('hybnet.rest_service');
        $settingEntityService = $this->getServiceContainer()->get('hybnet.entityService.setting');

        $restCallParameters = array(
            'address' => $settingEntityService->getValueByName('controller_ip'),
            'method' => RestCall::METHOD_GET,
            'port' => $settingEntityService->getValueByName('controller_port'),
            'path' => self::OPENFLOW_TOPOLOGY_DISCOVER_URL,
            'parameters' => array(),
            'username' => $settingEntityService->getValueByName('controller_username'),
            'password' => $settingEntityService->getValueByName('controller_password'),
        );

        $result = array();
        /* Controller sometimes returns false for no apparent reason */
        $repeatsLeft = 50;
        while(!$result && $repeatsLeft > 0) {
            $repeatsLeft--;
            $result = $restService->executeCallFromParametersArray($restCallParameters);

        }

        return $result;
    }

    /**
     * @param Object\Topology $topology
     * @param array $topologyInformation
     * @throws \Exception
     */
    private function addOpenFlowTopology(Object\Topology $topology, array $topologyInformation)
    {
        // Takes the first topology only
        $nodeInformation = $topologyInformation['network-topology']['topology'][0]['node'];
        $linkInformation = $topologyInformation['network-topology']['topology'][0]['link'];

        foreach ($nodeInformation as $node) {
            $info = json_decode($this->requestOpenFlowNodeInformation($node['node-id']), true);
            if (!$info) {
                throw new \Exception('OpenFlow node information missing.');
            }
            $this->addOpenFlowNode($topology, $info);
        }

        foreach ($linkInformation as $link) {
            $this->addOpenFlowLink($topology, $link);
        }
    }

    /**
     * @param Object\Topology $topology
     * @param array $nodeInformation
     */
    private function addOpenFlowNode(Object\Topology $topology, array $nodeInformation)
    {
        $nodeInformation = $nodeInformation['node'][0];

        $openFlowId = explode(':', $nodeInformation['id'])[1];
        $chassisId = $this->getServiceContainer()
            ->get('hybnet.core.utilityfunctions')
            ->openFlowIdToChassisId($openFlowId);

        $node = new Object\Node($topology, $chassisId);
        $node->setName($nodeInformation['id'])
            ->setIpAddress('')
            ->setType(Object\Node::TYPE_OPENFLOW);

        $nodeConnectorInformation = $nodeInformation['node-connector'];

        foreach ($nodeConnectorInformation as $nodeConnector) {
            $this->addOpenFlowNodeConnector(
                $topology,
                $node,
                $nodeConnector
            );

        }
    }

    /**
     * @param Object\Topology $topology
     * @param Object\Node $node
     * @param array $nodeConnectorInformation
     */
    private function addOpenFlowNodeConnector(Object\Topology $topology, Object\Node $node, array $nodeConnectorInformation)
    {
        $idArray = explode(':', $nodeConnectorInformation['id']);

        $interfaceName = $idArray[2];
        $chassisId = $this->getServiceContainer()
            ->get('hybnet.core.utilityfunctions')
            ->openFlowIdToChassisId($idArray[1]);

        $interfaceId = $chassisId . '-' . $interfaceName;
        $mac = $nodeConnectorInformation['flow-node-inventory:hardware-address'];

        $nodeConnector = new Object\NodeConnector($topology, $interfaceId);
        $nodeConnector->setMac($mac)
            ->setInterfaceName($interfaceName);

        $node->addNodeConnector($nodeConnector);
    }

    /**
     * @param Object\Topology $topology
     * @param array $linkInformation
     */
    private function addOpenFlowLink(Object\Topology $topology, array $linkInformation)
    {
        $sourceIdPieces = (explode(':', $linkInformation['source']['source-tp']));
        $destinationIdPieces = (explode(':', $linkInformation['destination']['dest-tp']));

        $sourceChassisId = $this->getServiceContainer()
            ->get('hybnet.core.utilityfunctions')
            ->openFlowIdToChassisId($sourceIdPieces[1]);

        $destinationChassisId = $this->getServiceContainer()
            ->get('hybnet.core.utilityfunctions')
            ->openFlowIdToChassisId($destinationIdPieces[1]);

        $sourceNodeConnectorId = $sourceChassisId . '-' . $sourceIdPieces[2];
        $destinationNodeConnectorId = $destinationChassisId . '-' . $destinationIdPieces[2];

        $sourceNodeConnector = $topology->getNodeConnector($sourceNodeConnectorId);
        $destinationNodeConnector = $topology->getNodeConnector($destinationNodeConnectorId);

        $this->addLink($sourceNodeConnector, $destinationNodeConnector);
    }

    /**
     * @param string $nodeId
     * @return bool|string
     */
    private function requestOpenFlowNodeInformation($nodeId)
    {
        $restService = $this->getServiceContainer()->get('hybnet.rest_service');
        $settingEntityService = $this->getServiceContainer()->get('hybnet.entityservice.setting');

        $restCallParameters = array(
            'address' => $settingEntityService->getValueByName('controller_ip'),
            'method' => 'GET',
            'port' => $settingEntityService->getValueByName('controller_port'),
            'path' => self::OPENFLOW_NODE_INFORMATION_URL . '/' . $nodeId,
            'parameters' => array(),
            'username' => $settingEntityService->getValueByName('controller_username'),
            'password' => $settingEntityService->getValueByName('controller_password'),
        );

        return $restService->executeCallFromParametersArray($restCallParameters);
    }

    /**
     * @param Object\NodeConnector $sourceConnector
     * @param Object\NodeConnector $destinationConnector
     * @param bool $overRideExisting
     */
    private function addLink(Object\NodeConnector $sourceConnector, Object\NodeConnector $destinationConnector, $overRideExisting = false)
    {
        if($sourceConnector->getEdgesTo($destinationConnector)->isEmpty()) {
            $edges = $destinationConnector->getEdgesOut();
            if ($edges->count() > 1 && $overRideExisting) {
                /** @var Graph\Edge\Undirected $edge */
                foreach ($edges as $edge) {
                    if ($edge->getVertexToFrom($destinationConnector) instanceof Object\NodeConnector) {
                        $edge->destroy();
                    }
                }
            }

            $sourceConnector->connectToAnother($destinationConnector);
        }
    }

    /**
     * @param Object\Topology $topology
     */
    private function addLegacyTopology(Object\Topology $topology)
    {
        $networkSwitches = $this->getServiceContainer()->get('hybnet.entityService.networkSwitch')->getAllPublished();
        foreach($networkSwitches as $networkSwitch) {
            $this->addLegacyNode($topology, $networkSwitch);
        }
    }

    /**
     * @param Object\Topology $topology
     * @param Entity\NetworkSwitch $networkSwitch
     */
    private function addLegacyNode(Object\Topology $topology, Entity\NetworkSwitch $networkSwitch)
    {
        $nodeInformation = $this->requestLegacyNodeInformation($networkSwitch);

        $legacyId = $nodeInformation['localSystemNameInfo'][0]['value'];
        $chassisId = $nodeInformation['localChassisIdInfo'][0]['value'];

        $node = new Object\Node($topology, $chassisId);
        $node->setName($legacyId)
            ->setIpAddress($networkSwitch->getIpAddress())
            ->setType(Object\Node::TYPE_LEGACY);

        $utilityFunctions = $this->getServiceContainer()
            ->get('hybnet.core.utilityfunctions');

        $remoteChassisIdInformation = $utilityFunctions->flattenSNMPResultArray($nodeInformation['remoteChassisIdInfo']);
        $remotePortIdInformation = $utilityFunctions->flattenSNMPResultArray($nodeInformation['remotePortIdInfo']);
        $localPortIdInformation = $utilityFunctions->flattenSNMPResultArray($nodeInformation['localPortIdInfo']);

        foreach ($remoteChassisIdInformation as $oid => $remoteChassisId) {
            $remotePortOid = str_replace(self::SNMP_LLDP_REM_MAC_OID, self::SNMP_LLDP_REM_PORT_ID_OID, $oid);
            $remotePortId = $remotePortIdInformation[$remotePortOid];

            $internalNumber = explode('.', str_replace(self::SNMP_LLDP_REM_MAC_OID . '.0.', '', $oid))[0];
            $localPortOid = self::SNMP_LLDP_LOC_PORT_ID_OID . '.' . $internalNumber;
            $localPortId = $localPortIdInformation[$localPortOid];

            $this->addLegacyNodeConnector(
                $topology,
                $node,
                $chassisId,
                $localPortId,
                $remoteChassisId,
                $remotePortId
            );
        }
    }

    /**
     * @param Object\Topology $topology
     * @param Object\Node $node
     * @param String $chassisId
     * @param String $localPortId
     * @param String $remoteChassisId
     * @param String $remotePortId
     */
    private function addLegacyNodeConnector(Object\Topology $topology, Object\Node $node, $chassisId, $localPortId, $remoteChassisId, $remotePortId)
    {
        $interfaceId = $chassisId . '-' . $localPortId;
        $mac = '';

        $nodeConnector = new Object\NodeConnector($topology, $interfaceId);
        $nodeConnector->setMac($mac)
            ->setInterfaceName($localPortId);

        $node->addNodeConnector($nodeConnector);

        $remoteInterfaceId = $remoteChassisId . '-' . $remotePortId;

        $destinationNodeConnector = $topology->getNodeConnector($remoteInterfaceId);

        if ($destinationNodeConnector) {
            $this->addLink($nodeConnector, $destinationNodeConnector, true);
        }
    }

    /**
     * @param Entity\NetworkSwitch $networkSwitch
     * @return array
     * @throws \Exception
     */
    public function requestLegacyNodeInformation(Entity\NetworkSwitch $networkSwitch)
    {
        $snmpService = $this->getServiceContainer()->get('hybnet.snmp_request_service');
        $settingEntityService = $this->getServiceContainer()->get('hybnet.entityService.setting');

        $snmpRequestParameters = array(
            'address' => $networkSwitch->getIpAddress(),
            'method' => SnmpRequest::METHOD_GET,
            'type' => SnmpRequest::TYPE_GET_BULK,
            'community' => $settingEntityService->getValueByName('snmp_community'),
        );

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_REM_MAC_OID;
        $remoteChassisIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);
        if (!$remoteChassisIdInfo['output']['results']) {
            throw new \Exception('Remote chassis id info missing');
        }

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_REM_PORT_ID_OID;
        $remotePortIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);
        if (!$remotePortIdInfo['output']['results']) {
            throw new \Exception('Remote port id info missing');
        }

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_LOC_PORT_ID_OID;
        $localPortIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);
        if (!$localPortIdInfo['output']['results']) {
            throw new \Exception('Local port id info missing');
        }

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_LOC_PORT_MAC_OID;
        $localChassisIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);
        if (!$localChassisIdInfo['output']['results']) {
            throw new \Exception('Local chassis id info missing');
        }

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_LOC_NAME_OID;
        $localSystemNameInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);
        if (!$localSystemNameInfo['output']['results']) {
            throw new \Exception('Local system name info missing');
        }

        return array(
            'remoteChassisIdInfo' => $remoteChassisIdInfo['output']['results'],
            'remotePortIdInfo' => $remotePortIdInfo['output']['results'],
            'localPortIdInfo' => $localPortIdInfo['output']['results'],
            'localChassisIdInfo' => $localChassisIdInfo['output']['results'],
            'localSystemNameInfo' => $localSystemNameInfo['output']['results'],
        );
    }


}
