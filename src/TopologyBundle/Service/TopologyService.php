<?php

namespace TopologyBundle\Service;

use CoreBundle\Service\AbstractService;
use TopologyBundle\Object\Topology;
use TopologyBundle\Object\Node;
use TopologyBundle\Object\Link;
use TopologyBundle\Object\NodeConnector;
use SNMPBundle\Object\SnmpRequest;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class TopologyService
 * @package TopologyBundle\Service
 * @deprecated use TopologyBundle2 instead
 */
class TopologyService extends AbstractService
{
    const OPENFLOW_TOPOLOGY_DISCOVER_URL = '/restconf/operational/network-topology:network-topology';
    const OPENFLOW_NODE_INFORMATION_URL = '/restconf/operational/opendaylight-inventory:nodes/node';

    const SNMP_LLDP_REM_PORT_ID_OID = '1.0.8802.1.1.2.1.4.1.1.7';
    const SNMP_LLDP_REM_MAC_OID = '1.0.8802.1.1.2.1.4.1.1.5';

    const SNMP_LLDP_LOC_PORT_ID_OID = '1.0.8802.1.1.2.1.3.7.1.3';
    const SNMP_LLDP_LOC_PORT_MAC_OID = '1.0.8802.1.1.2.1.3.2';

    const SNMP_LLDP_LOC_NAME_OID = '1.0.8802.1.1.2.1.3.3';

    const VLAN_HEX = '8D4'; // 2260 vlan


    /**
     * @return Topology
     */
    public function discoverTopology()
    {
        $openFlowTopology = $this->discoverOpenFlowTopology();
        $legacyTopology = $this->discoverLegacyTopology();

        $globalTopology = $this->mergeTopologies($openFlowTopology, $legacyTopology);

        return $globalTopology;
    }

    /**
     * @return bool|string
     */
    private function requestOpenFlowTopologyInformation()
    {
        $restService = $this->getServiceContainer()->get('hybnet.rest_service');
        $settingEntityService = $this->getServiceContainer()->get('hybnet.entityService.setting');

        $restCallParameters = array(
            'address' => $settingEntityService->getValueByName('controller_ip'),
            'method' => 'GET',
            'port' => $settingEntityService->getValueByName('controller_port'),
            'path' => self::OPENFLOW_TOPOLOGY_DISCOVER_URL,
            'parameters' => array(),
            'username' => $settingEntityService->getValueByName('controller_username'),
            'password' => $settingEntityService->getValueByName('controller_password'),
        );

        return $restService->executeCallFromParametersArray($restCallParameters);
    }

    /**
     * @param string $nodeId
     * @return bool|string
     */
    private function requestOpenFlowNodeInformation($nodeId)
    {
        $restService = $this->getServiceContainer()->get('hybnet.rest_service');
        $settingEntityService = $this->getServiceContainer()->get('hybnet.entityService.setting');

        $restCallParameters = array(
            'address' => $settingEntityService->getValueByName('controller_ip'),
            'method' => 'GET',
            'port' => $settingEntityService->getValueByName('controller_port'),
            'path' => self::OPENFLOW_NODE_INFORMATION_URL . '/' . $nodeId,
            'parameters' => array(),
            'username' => $settingEntityService->getValueByName('controller_username'),
            'password' => $settingEntityService->getValueByName('controller_password'),
        );

        return $restService->executeCallFromParametersArray($restCallParameters);
    }

    /**
     * @param array $topologyInformation
     * @return Topology
     */
    private function parseOpenFlowTopologyInformation(array $topologyInformation)
    {
        // Takes the first topology only
        $nodeInformation = $topologyInformation['network-topology']['topology'][0]['node'];
        $linkInformation = $topologyInformation['network-topology']['topology'][0]['link'];

        $nodes = array();
        foreach ($nodeInformation as $node) {
            $nodes[$node['node-id']] = $this->parseOpenFlowNodeInformation(json_decode($this->requestOpenFlowNodeInformation($node['node-id']), true));
        }

        $links = array();
        foreach ($linkInformation as $link) {
            $links[$link['link-id']] = $this->parseOpenFlowLinkInformation($link, $nodes);
        }

        $topology = new Topology();
        $topology->setNodes($nodes)
            ->setLinks($links);

        return $topology;
    }

    /**
     * @param array $nodeInformation
     * @return Node
     */
    private function parseOpenFlowNodeInformation(array $nodeInformation)
    {
        $nodeInformation = $nodeInformation['node'];
        $node = new Node();
        $node->setName($nodeInformation[0]['id'])
            ->setAddress('')
            ->setType(Node::TYPE_OPENFLOW);

        $openFlowId = explode(':', $nodeInformation[0]['id'])[1];
        $unsplitMac = substr(dechex($openFlowId), -12);
        $chunks = str_split($unsplitMac, 2);
        $chassisId = implode(':', $chunks);
        $node->setChassisId($chassisId);

        $nodeConnectorInformation =  $nodeInformation[0]['node-connector'];
        $nodeConnectors = $this->parseOpenFlowNodeConnectorInformation($nodeConnectorInformation);
        $node->setNodeConnectors($nodeConnectors);

        return $node;
    }

    /**
     * @param array $nodeConnectorInformation
     * @return array
     */
    private function parseOpenFlowNodeConnectorInformation(array $nodeConnectorInformation)
    {
        $nodeConnectors = array();
        foreach ($nodeConnectorInformation as $dataPiece) {
            if (explode(':', $dataPiece['id'])[2] != 'LOCAL') {
                $nodeConnector = new NodeConnector();
                $nodeConnector->setInterfaceId($dataPiece['id']);
                $nodeConnector->setMac($dataPiece['flow-node-inventory:hardware-address']);
                $nodeConnectors[$dataPiece['id']] = $nodeConnector;
            }
        }
        return $nodeConnectors;
    }

    /**
     * @param array $linkInfo
     * @param array $nodes
     * @return Link
     */
    private function parseOpenFlowLinkInformation(array $linkInfo, array $nodes)
    {
        $link = new Link();
        $link->setNode1($linkInfo['source']['source-node']);
        $link->setNodeConnector1($linkInfo['source']['source-tp']);
        $link->setMac1($this->findMac($nodes, $linkInfo['source']['source-node'], $linkInfo['source']['source-tp']));

        $openFlowId = explode(':', $linkInfo['source']['source-node'])[1];
        $unsplitMac = substr(dechex($openFlowId), -12);
        $chunks = str_split($unsplitMac, 2);
        $chassisId = implode(':', $chunks);
        $link->setChassisId1($chassisId);

        $link->setNode2($linkInfo['destination']['dest-node']);
        $link->setNodeConnector2($linkInfo['destination']['dest-tp']);
        $link->setMac2($this->findMac($nodes, $linkInfo['destination']['dest-node'], $linkInfo['destination']['dest-tp']));

        $openFlowId = explode(':', $linkInfo['destination']['dest-node'])[1];
        $unsplitMac = substr(dechex($openFlowId), -12);
        $chunks = str_split($unsplitMac, 2);
        $chassisId = implode(':', $chunks);
        $link->setChassisId2($chassisId);

        return $link;
    }

    /**
     * @param array|Node[] $nodes
     * @param string $nodeId
     * @param string $nodeConnectorId
     * @return string
     */
    private function findMac(array $nodes, $nodeId, $nodeConnectorId)
    {
        $node = $nodes[$nodeId];
        $nodeConnectors = $node->getNodeConnectors();
        $nodeConnector = $nodeConnectors[$nodeConnectorId];

        return $nodeConnector->getMac();
    }

    /**
     * @return Topology
     */
    public function discoverOpenFlowTopology()
    {
        $topologyInformation = $this->requestOpenFlowTopologyInformation();
        $openFlowPseudoTopology = $this->parseOpenFlowTopologyInformation(json_decode($topologyInformation, true));
        return $openFlowPseudoTopology;
    }

    /**
     * @param string $ipAddress
     * @return array
     */
    public function requestLegacyNodeInformation($ipAddress)
    {
        $snmpService = $this->getServiceContainer()->get('hybnet.snmp_request_service');
        $settingEntityService = $this->getServiceContainer()->get('hybnet.entityService.setting');

        $snmpRequestParameters = array(
            'address' => $ipAddress,
            'method' => SnmpRequest::METHOD_GET,
            'type' => SnmpRequest::TYPE_GET_BULK,
            'oid' => self::SNMP_LLDP_REM_MAC_OID,
            'community' => $settingEntityService->getValueByName('snmp_community'),
        );

        $remoteChassisIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_REM_PORT_ID_OID;

        $remotePortIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_LOC_PORT_ID_OID;

        $localPortIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_LOC_PORT_MAC_OID;

        $localChassisIdInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);

        $snmpRequestParameters['oid'] = self::SNMP_LLDP_LOC_NAME_OID;

        $localSystemNameInfo = json_decode($snmpService->executeCallFromParametersArray($snmpRequestParameters), true);


        $node = new Node();
        $node->setAddress($ipAddress);
        $node->setName($localSystemNameInfo['output']['results'][0]['value']);
        $node->setType(Node::TYPE_LEGACY);
        $node->setChassisId($localChassisIdInfo['output']['results'][0]['value']);

        //Node connectors
        $nodeConnectors = array();
        $links = array();

        $remoteConnections = $remoteChassisIdInfo['output']['results'];
        $remotePorts = $remotePortIdInfo['output']['results'];
        $localPorts = $localPortIdInfo['output']['results'];
        foreach ($remoteConnections as $remoteConnection) {
            $link = new Link();

            $link->setMac1($remoteConnection['value']);
            $link->setChassisId1($remoteConnection['value']);

            $link->setMac2($localChassisIdInfo['output']['results'][0]['value']);
            $link->setChassisId2($localChassisIdInfo['output']['results'][0]['value']);
            $links[] = $link;
        }

        return array('links' => $links, 'node' => $node);
    }

    /**
     * @return Topology
     */
    public function discoverLegacyTopology()
    {
        $legacyNodes = $this->getServiceContainer()->get('hybnet.entityService.networkSwitch')->getAll();
        $links = array();
        $nodes = array();
        foreach($legacyNodes as $legacyNode) {
            $result = $this->requestLegacyNodeInformation($legacyNode->getIpAddress());
            $links = array_merge($links, $result['links']);
            $nodes[$result['node']->getName()] = $result['node'];
        }

        $topology = new Topology();
        $topology->setNodes($nodes);
        $topology->setLinks($links);

        return $topology;
    }

    /**
     * @param Topology $openFlowTopology
     * @param Topology $legacyTopology
     * @return Topology
     */
    private function mergeTopologies(Topology $openFlowTopology, Topology $legacyTopology)
    {
        $globalTopology = new Topology();

        $nodes = array_merge($openFlowTopology->getNodes(), $legacyTopology->getNodes());
        $globalTopology->setNodes($nodes);

        $openFlowLinks = $openFlowTopology->getLinks();
        $legacyLinks = $legacyTopology->getLinks();

        $links = array_merge($openFlowLinks, $legacyLinks);

        $globalTopology->setLinks($links);

        return $globalTopology;
    }
}
