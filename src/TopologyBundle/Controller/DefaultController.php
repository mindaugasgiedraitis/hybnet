<?php

namespace TopologyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class DefaultController
 * @package TopologyBundle\Controller
 * @deprecated use TopologyBundle2 instead
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function topologyDiscoverAjaxAction(Request $request)
    {
        try {
            $topology = $this->get('hybnet.topology_service')->discoverTopology();
            $response = new Response(json_encode($topology->getArrayForDisplay()));
        } catch (\Exception $e) {
            $response = new Response($e->getMessage());
        }
        return $response;
    }
}
