<?php

namespace TopologyBundle\Object;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class Link
 * @package TopologyBundle\Object
 * @deprecated use TopologyBundle2 instead
 */
class Link
{
    /**
     * @var Node $node1
     */
    protected $node1;

    /**
     * @var NodeConnector $nodeConnector1
     */
    protected $nodeConnector1;

    /**
     * @var string $mac1
     */
    protected $mac1;

    /**
     * @var string $chassisId1
     */
    protected $chassisId1;

    /**
     * @var Node $node2
     */
    protected $node2;

    /**
     * @var NodeConnector $nodeConnector2
     */
    protected $nodeConnector2;

    /**
     * @var string $mac2
     */
    protected $mac2;

    /**
     * @var string $chassisId2
     */
    protected $chassisId2;

    /**
     * @return Node|string
     */
    public function getNode1()
    {
        return $this->node1;
    }

    /**
     * @param Node|string $node1
     * @return Link
     */
    public function setNode1($node1)
    {
        $this->node1 = $node1;
        return $this;
    }

    /**
     * @return NodeConnector|string
     */
    public function getNodeConnector1()
    {
        return $this->nodeConnector1;
    }

    /**
     * @param NodeConnector|string $nodeConnector1
     * @return Link
     */
    public function setNodeConnector1($nodeConnector1)
    {
        $this->nodeConnector1 = $nodeConnector1;
        return $this;
    }

    /**
     * @return string
     */
    public function getMac1()
    {
        return $this->mac1;
    }

    /**
     * @param string $mac1
     * @return Link
     */
    public function setMac1($mac1)
    {
        $this->mac1 = $mac1;
        return $this;
    }

    /**
     * @return string
     */
    public function getChassisId1()
    {
        return $this->chassisId1;
    }

    /**
     * @param string $chassisId1
     * @return Link
     */
    public function setChassisId1($chassisId1)
    {
        $this->chassisId1 = $chassisId1;
        return $this;
    }

    /**
     * @return Node|string
     */
    public function getNode2()
    {
        return $this->node2;
    }

    /**
     * @param Node|string $node2
     * @return Link
     */
    public function setNode2($node2)
    {
        $this->node2 = $node2;
        return $this;
    }

    /**
     * @return NodeConnector|string
     */
    public function getNodeConnector2()
    {
        return $this->nodeConnector2;
    }

    /**
     * @param NodeConnector|string $nodeConnector2
     * @return Link
     */
    public function setNodeConnector2($nodeConnector2)
    {
        $this->nodeConnector2 = $nodeConnector2;
        return $this;
    }

    /**
     * @return string
     */
    public function getMac2()
    {
        return $this->mac2;
    }

    /**
     * @param string $mac2
     * @return Link
     */
    public function setMac2($mac2)
    {
        $this->mac2 = $mac2;
        return $this;
    }

    /**
     * @return string
     */
    public function getChassisId2()
    {
        return $this->chassisId2;
    }

    /**
     * @param string $chassisId2
     * @return Link
     */
    public function setChassisId2($chassisId2)
    {
        $this->chassisId2 = $chassisId2;
        return $this;
    }

}
