<?php

namespace TopologyBundle\Object;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class NodeConnector
 * @package TopologyBundle\Object
 * @deprecated use TopologyBundle2 instead
 */
class NodeConnector
{
    /**
     * @var string $interfaceId
     */
    protected $interfaceId;

    /**
     * @var string $mac
     */
    protected $mac;

    /**
     * @return string
     */
    public function getInterfaceId()
    {
        return $this->interfaceId;
    }

    /**
     * @param string $interfaceId
     * @return NodeConnector
     */
    public function setInterfaceId($interfaceId)
    {
        $this->interfaceId = $interfaceId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMac()
    {
        return $this->mac;
    }

    /**
     * @param string $mac
     * @return NodeConnector
     */
    public function setMac($mac)
    {
        $this->mac = $mac;
        return $this;
    }


}
