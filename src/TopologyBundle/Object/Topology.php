<?php

namespace TopologyBundle\Object;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class Topology
 * @package TopologyBundle\Object
 * @deprecated use TopologyBundle2 instead
 */
class Topology
{
    /**
     * @var array|Node[] $nodes
     */
    protected $nodes = array();

    /**
     * @var array|Link[] $links
     */
    protected $links = array();

    /**
     * @return array|Node[]
     */
    public function getNodes()
    {
        return $this->nodes;
    }

    /**
     * @param array|Node[] $nodes
     * @return Topology
     */
    public function setNodes($nodes)
    {
        $this->nodes = $nodes;
        return $this;
    }

    /**
     * @return array|Link[]
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * @param array|Link[] $links
     * @return Topology
     */
    public function setLinks($links)
    {
        $this->links = $links;
        return $this;
    }

    /**
     * Custom methods
     */

    /**
     * {id: 1, label: 'Node 1'},
     * {id: 2, label: 'Node 2'},
     * {id: 3, label: 'Node 3'},
     * {id: 4, label: 'Node 4'},
     * {id: 5, label: 'Node 5'}
     *
     * {from: 1, to: 3},
     * {from: 1, to: 2},
     * {from: 2, to: 4},
     * {from: 2, to: 5}
     */

    /**
     * @return array
     */
    public function getArrayForDisplay()
    {
        $nodeIds = array();
        $nodes = $this->getNodes();

        foreach ($nodes as $node) {
            $nodeIds[] = array(
                'id' => $node->getChassisId(),
                'label' => $node->getName(),
            );
        }

        $linkIds = array();
        $links = $this->getLinks();

        foreach ($links as $link) {
            $linkIds[] = array(
                'from' => $link->getChassisId1(),
                'to' => $link->getChassisId2(),
            );
        }

        return array(
            'nodes' => $nodeIds,
            'edges' => $linkIds,
        );
    }
}
