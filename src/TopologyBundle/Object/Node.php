<?php

namespace TopologyBundle\Object;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class Node
 * @package TopologyBundle\Object
 * @deprecated use TopologyBundle2 instead
 */
class Node
{
    const TYPE_OPENFLOW = 'OPENFLOW';
    const TYPE_LEGACY = 'LEGACY';

    /**
     * @var string $name
     */
    protected $name;

    /**
     * @var string $address
     */
    protected $address;

    /**
     * @var string $type
     */
    protected $type;

    /**
     * @var NodeConnector[]|array
     */
    protected $nodeConnectors;

    /**
     * @var string $chassisId
     */
    protected $chassisId;

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Node
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Node
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Node
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return array|NodeConnector[]
     */
    public function getNodeConnectors()
    {
        return $this->nodeConnectors;
    }

    /**
     * @param array|NodeConnector[] $nodeConnectors
     * @return Node
     */
    public function setNodeConnectors($nodeConnectors)
    {
        $this->nodeConnectors = $nodeConnectors;
        return $this;
    }

    /**
     * @return string
     */
    public function getChassisId()
    {
        return $this->chassisId;
    }

    /**
     * @param string $chassisId
     * @return Node
     */
    public function setChassisId($chassisId)
    {
        $this->chassisId = $chassisId;
        return $this;
    }

}
