<?php

namespace RestBundle\Object;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class RestCall
 * @package RestBundle\Object
 */
class RestCall
{
    const METHOD_GET = 'GET';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';

    /**
     * @var string $address
     */
    protected $address;

    /**
     * @var string $method
     */
    protected $method = self::METHOD_POST;

    /**
     * @var int $port
     */
    protected $port = 8181;

    /**
     * @var string $path
     */
    protected $path;

    /**
     * @var string $url
     */
    protected $url;

    /**
     * @var array $parameters
     */
    protected $parameters;

    /**
     * @var string $username
     */
    protected $username;

    /**
     * @var string $password
     */
    protected $password;

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return RestCall
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return RestCall
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param int $port
     * @return RestCall
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     * @return RestCall
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return RestCall
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return RestCall
     */
    public function setParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return RestCall
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return RestCall
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /* Custom methods */
    /**
     * @return array
     */
    public static function getAllMethods()
    {
        return array(
            self::METHOD_GET,
            self::METHOD_POST,
            self::METHOD_PUT,
        );
    }

    /**
     * @return string
     */
    public function createUrl()
    {
        if ($this->getUrl()) {
            return $this->getUrl();
        }

        if (!$this->getAddress()) {
            return '';
        }
        $url = $this->getAddress();
        if ($this->getPort()) {
            $url .= ':' . $this->getPort();
        }

        if ($this->getPath()) {
            if (strpos($this->getPath(), "/") === 0) {
                $url .= $this->getPath();
            } else {
                $url .= '/' . $this->getPath();
            }
        }

        $this->setUrl($url);
        return $url;
    }

    /**
     * @TODO May have additional return types. Check.
     * @return string|bool
     */
    public function execute()
    {
        $curl = curl_init();
        $url = $this->createUrl();

        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $this->getMethod());

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        if (count($this->getParameters()) > 0) {
            $parameters = array();
            foreach ($this->getParameters() as $parameterName => $parameterValue) {
                $parameters[$parameterName] = $parameterValue;
            }
            $dataString = json_encode($parameters);

            curl_setopt($curl, CURLOPT_POSTFIELDS, $dataString);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($dataString)
                ));
        } else {
            curl_setopt($curl, CURLOPT_HTTPHEADER, array(
                    'Accept: application/json',
                    'Content-Type: application/json',
                ));
        }

        // Optional Authentication:
        if ($this->getUsername() && $this->getPassword()) {
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_USERPWD, $this->getUsername() . ':' . $this->getPassword());
        }

        curl_setopt($curl, CURLOPT_URL, $url);

        $result = curl_exec($curl);

        curl_close($curl);

        return $result;
    }
}