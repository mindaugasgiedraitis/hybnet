<?php

namespace RestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class DefaultController
 * @package RestBundle\Controller
 */
class DefaultController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     */
    public function ajaxCallAction(Request $request)
    {
        $restCallParameters = $request->get('restCall');

        try {
            $response = new Response($this->get('hybnet.rest_service')->executeCallFromParametersArray($restCallParameters));
        } catch (\Exception $e) {
            $response = new Response($e->getMessage());
        }
        return $response;
    }
}
