<?php

namespace RestBundle\Service;

use CoreBundle\Service\AbstractService;
use RestBundle\Object\RestCall;
use SNMPBundle\Object\SnmpRequest;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class RestService
 * @package RestBundle\Service
 */
class RestService extends AbstractService
{
    /**
     * @var string $serviceClass
     */
    protected $serviceClass = 'RestBundle\Object\RestCall';

    /**
     * @param string $serviceClass
     * @return RestService
     */
    public function setServiceClass($serviceClass)
    {
        $this->serviceClass = $serviceClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceClass()
    {
        return $this->serviceClass;
    }

    /**
     * @return array
     */
    public function getAllMethods()
    {
        /** @var RestCall|string $serviceClass */
        $serviceClass = $this->getServiceClass();
        return $serviceClass::getAllMethods();
    }

    /**
     * @param array $restCallParameters
     * @return bool|string
     * @throws \Exception
     */
    public function executeCallFromParametersArray(array $restCallParameters)
    {
        $address = isset($restCallParameters['address']) ? $restCallParameters['address'] : '';
        $method = isset($restCallParameters['method']) ? $restCallParameters['method'] : '';
        $port = isset($restCallParameters['port']) ? $restCallParameters['port'] : '';
        $path = isset($restCallParameters['path']) ? $restCallParameters['path'] : '';
        $url = isset($restCallParameters['url']) ? $restCallParameters['url'] : '';
        $parameters = isset($restCallParameters['parameters']) ? $restCallParameters['parameters'] : array();
        $username = isset($restCallParameters['username']) ? $restCallParameters['username'] : '';
        $password = isset($restCallParameters['password']) ? $restCallParameters['password'] : '';
        if (!$url && !$address) {
            throw new \Exception('No url or address defined');
        }

        $restCall = new RestCall();
        $restCall->setAddress($address)
            ->setMethod($method)
            ->setPort($port)
            ->setPath($path)
            ->setUrl($url)
            ->setParameters($parameters)
            ->setUsername($username)
            ->setPassword($password);

        return $restCall->execute();
    }

    /**
     * @param SnmpRequest $snmpRequest
     * @return bool|string
     */
    public function executeRestCallBySnmpRequest(SnmpRequest $snmpRequest)
    {
        $restCall = new RestCall();
        $restCall->setAddress($this->getServiceContainer()->get('hybnet.entityService.setting')->getValueByName('controller_ip'))
            ->setPath($snmpRequest->getMethod())
            ->setParameters($snmpRequest->createRestParameters())
            ->setPort($this->getServiceContainer()->get('hybnet.entityService.setting')->getValueByName('controller_port'))
            ->setUsername($this->getServiceContainer()->get('hybnet.entityService.setting')->getValueByName('controller_username'))
            ->setPassword($this->getServiceContainer()->get('hybnet.entityService.setting')->getValueByName('controller_password'));

        return $restCall->execute();
    }
}