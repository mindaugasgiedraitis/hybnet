<?php

namespace SNMPBundle\Object;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class SnmpRequest
 * @package SNMPBundle\Object
 */
class SnmpRequest
{
    const METHOD_GET = '/restconf/operations/snmp:snmp-get';
    const METHOD_SET = '/restconf/operations/snmp:snmp-set';

    const TYPE_GET = 'GET';
    const TYPE_GET_NEXT = 'GET-NEXT';
    const TYPE_GET_BULK = 'GET-BULK';

    /**
     * @var string $method
     */
    protected $method = self::METHOD_GET;

    /**
     * @var string $type
     */
    protected $type = self::TYPE_GET;

    /**
     * @var string $address
     */
    protected $address;

    /**
     * @var string $oid
     */
    protected $oid;

    /**
     * @var string $community
     */
    protected $community;

    /**
     * @var string $value
     */
    protected $value;

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return SnmpRequest
     */
    public function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return SnmpRequest
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return SnmpRequest
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getOid()
    {
        return $this->oid;
    }

    /**
     * @param string $oid
     * @return SnmpRequest
     */
    public function setOid($oid)
    {
        $this->oid = $oid;
        return $this;
    }

    /**
     * @return string
     */
    public function getCommunity()
    {
        return $this->community;
    }

    /**
     * @param string $community
     * @return SnmpRequest
     */
    public function setCommunity($community)
    {
        $this->community = $community;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return SnmpRequest
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Custom methods
     */

    /**
     * @return array
     */
    public static function getAllMethods()
    {
        return array(
            'GET' => self::METHOD_GET,
            'SET' =>self::METHOD_SET,
        );
    }

    /**
     * @return array
     */
    public static function getAllTypes()
    {
        return array(
            self::TYPE_GET,
            self::TYPE_GET_BULK,
            self::TYPE_GET_NEXT,
        );
    }

    /**
     * @return array
     */
    public function createRestParameters()
    {
        $params = array(
            "ip-address" => $this->getAddress(),
            "oid" => $this->getOid(),
            "community" => $this->getCommunity()
        );

        if ($this->getMethod() == self::METHOD_GET) {
            $params["get-type"] = $this->getType();
        } elseif ($this->getMethod() == self::METHOD_SET) {
            $params["value"] = $this->getValue();
        }

        return array('input' => $params);
    }
}