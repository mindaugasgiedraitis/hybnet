<?php

namespace SNMPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class DefaultController
 * @package SNMPBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function ajaxCallAction(Request $request)
    {
        $snmpRequestParameters = $request->get('snmpRequest');

        try {
            $response = new Response($this->get('hybnet.snmp_request_service')->executeCallFromParametersArray($snmpRequestParameters));
        } catch (\Exception $e) {
            $response = new Response($e->getMessage());
        }
        return $response;
    }
}
