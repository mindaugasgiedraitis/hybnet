<?php

namespace SNMPBundle\Service;

use CoreBundle\Service\AbstractService;
use SNMPBundle\Object\SnmpRequest;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class SnmpRequestService
 * @package SNMPBundle\Service
 */
class SnmpRequestService extends AbstractService
{
    /**
     * @var string $serviceClass
     */
    protected $serviceClass = 'SNMPBundle\Object\SnmpRequest';

    /**
     * @param string $serviceClass
     * @return SnmpRequestService
     */
    public function setServiceClass($serviceClass)
    {
        $this->serviceClass = $serviceClass;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceClass()
    {
        return $this->serviceClass;
    }

    /**
     * @return array
     */
    public function getAllTypes()
    {
        /** @var SnmpRequest|string $serviceClass */
        $serviceClass = $this->getServiceClass();
        return $serviceClass::getAllTypes();
    }

    /**
     * @return array
     */
    public function getAllMethods()
    {
        /** @var SnmpRequest|string $serviceClass */
        $serviceClass = $this->getServiceClass();
        return $serviceClass::getAllMethods();
    }

    /**
     * @param array $restCallParameters
     * @return bool|string
     */
    public function executeCallFromParametersArray(array $restCallParameters)
    {
        $address = isset($restCallParameters['address']) ? $restCallParameters['address'] : '';
        $method = isset($restCallParameters['method']) ? $restCallParameters['method'] : '';
        $type = isset($restCallParameters['type']) ? $restCallParameters['type'] : '';
        $oid = isset($restCallParameters['oid']) ? $restCallParameters['oid'] : '';
        $community = isset($restCallParameters['community']) ? $restCallParameters['community'] : '';
        $value = isset($restCallParameters['value']) ? $restCallParameters['value'] : '';

        $snmpRequest = new SnmpRequest();
        $snmpRequest->setAddress($address)
            ->setMethod($method)
            ->setType($type)
            ->setOid($oid)
            ->setCommunity($community)
            ->setValue($value);

        return $this->getServiceContainer()->get('hybnet.rest_service')->executeRestCallBySnmpRequest($snmpRequest);
    }
}