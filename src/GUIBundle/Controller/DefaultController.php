<?php

namespace GUIBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class DefaultController
 * @package GUIBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('GUIBundle:Default:index.html.twig');
    }

    /**
     * @return Response
     */
    public function topMenuAction()
    {
        $menuItems = array(
            'main' => 'Main',
            'measure' => 'Measure',
            'snmp' => 'SNMP',
            'config' => 'Config',
            'devices' => 'Devices',
            'console' => 'Console',
            'oids' => 'OIDs',
        );

        return $this->render('GUIBundle:Default:topMenu.html.twig', array(
                'menuItems' => $menuItems,
            ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function mainTopMenuAction(Request $request)
    {
        return $this->render('GUIBundle:Default:mainTopMenu.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function measureTopMenuAction(Request $request)
    {
        return $this->render('GUIBundle:Default:measureTopMenu.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function snmpTopMenuAction(Request $request)
    {
        $snmpCommunity = $this->get('hybnet.entityservice.setting')->getValueByName('snmp_community');
        $snmpRequestMethods = $this->get('hybnet.snmp_request_service')->getAllMethods();
        $snmpGetTypes = $this->get('hybnet.snmp_request_service')->getAllTypes();

        return $this->render('GUIBundle:Default:snmpTopMenu.html.twig', array(
            'snmpCommunity' => $snmpCommunity,
            'snmpRequestMethods' => $snmpRequestMethods,
            'snmpGetTypes' => $snmpGetTypes,
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function consoleTopMenuAction(Request $request)
    {
        $methods = $this->get('hybnet.rest_service')->getAllMethods();
        $controllerIp = $this->get('hybnet.entityService.setting')->getValueByName('controller_ip');
        $controllerPort = $this->get('hybnet.entityService.setting')->getValueByName('controller_port');
        $controllerUsername = $this->get('hybnet.entityService.setting')->getValueByName('controller_username');
        $controllerPassword = $this->get('hybnet.entityService.setting')->getValueByName('controller_password');

        return $this->render('GUIBundle:Default:consoleTopMenu.html.twig', array(
                'methods' => $methods,
                'controllerIp' => $controllerIp,
                'controllerPort' => $controllerPort,
                'controllerUsername' => $controllerUsername,
                'controllerPassword' => $controllerPassword,
            ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function configTopMenuAction(Request $request)
    {
        $controllerIp = $this->get('hybnet.entityService.setting')->getValueByName('controller_ip');
        $controllerPort = $this->get('hybnet.entityService.setting')->getValueByName('controller_port');
        $controllerUsername = $this->get('hybnet.entityService.setting')->getValueByName('controller_username');
        $controllerPassword = $this->get('hybnet.entityService.setting')->getValueByName('controller_password');
        $snmpCommunity = $this->get('hybnet.entityService.setting')->getValueByName('snmp_community');

        return $this->render('GUIBundle:Default:configTopMenu.html.twig', array(
                'controllerIp' => $controllerIp,
                'controllerPort' => $controllerPort,
                'controllerUsername' => $controllerUsername,
                'controllerPassword' => $controllerPassword,
                'snmpCommunity' => $snmpCommunity,
            ));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function ajaxConfigSaveAction(Request $request)
    {
        $controllerIp = $request->get('controllerIp');
        $controllerPort = $request->get('controllerPort');
        $controllerUsername = $request->get('controllerUsername');
        $controllerPassword = $request->get('controllerPassword');
        $snmpCommunity = $request->get('snmpCommunity');

        if ($controllerIp) {
            $this->get('hybnet.entityService.setting')->save(array(
                    'name' => 'controller_ip',
                    'type' => 'controller_info',
                    'value' => $controllerIp,
                ));
        }
        if ($controllerPort) {
            $this->get('hybnet.entityService.setting')->save(array(
                    'name' => 'controller_port',
                    'type' => 'controller_info',
                    'value' => $controllerPort,
                ));
        }
        if ($controllerUsername) {
            $this->get('hybnet.entityService.setting')->save(array(
                    'name' => 'controller_username',
                    'type' => 'controller_info',
                    'value' => $controllerUsername,
                ));
        }
        if ($controllerPassword) {
            $this->get('hybnet.entityService.setting')->save(array(
                    'name' => 'controller_password',
                    'type' => 'controller_info',
                    'value' => $controllerPassword,
                ));
        }
        if ($snmpCommunity) {
            $this->get('hybnet.entityService.setting')->save(array(
                    'name' => 'snmp_community',
                    'type' => 'snmp_info',
                    'value' => $snmpCommunity,
                ));
        }
        $response = new JsonResponse();
        $response->setData('ok');
        return $response;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function oidsTopMenuAction(Request $request)
    {
        return $this->render('GUIBundle:Default:oidsTopMenu.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function devicesTopMenuAction(Request $request)
    {
        $devices = $this->get('hybnet.entityservice.networkswitch')->getAllPublished();

        return $this->render('GUIBundle:Default:devicesTopMenu.html.twig', array(
            'devices' => $devices,
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function saveNetworkSwitchAction(Request $request)
    {
        $params = array(
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'published' => $request->get('published') == 'true' ? true : false,
        );

        $response = array(
            'published' => $params['published'],
        );
        try {
            $switch = $this->get('hybnet.entityservice.networkswitch')->save($params);
            $response['status'] = 'success';
            $response['name'] = $switch->getName();
            $response['address'] = $switch->getIpAddress();
        } catch (\Exception $e) {
            $response['status'] = 'fail';
            $response['message'] = $e->getMessage();
        }

        return $this->render('GUIBundle:Default:jsonResponse.html.twig', array(
            'json' => json_encode($response, true),
        ));
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function ajaxGetTopMenuItemAction(Request $request)
    {
        $menuItemName = $request->get('menuItem');

        if ($menuItemName && method_exists($this, lcfirst($menuItemName) . 'TopMenuAction')) {
            return $this->{lcfirst($menuItemName) . 'TopMenuAction'}($request);
        }

        $response = new Response('Menu Item Not found');
        $response->setStatusCode(404);
        return $response;
    }
}
