<?php

namespace MeasurementBundle\Service;

use MeasurementBundle\Object;
use CoreBundle\Service\AbstractService;
use Symfony\Component\Stopwatch\Stopwatch;


/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class MeasurementService
 * @package MeasurementBundle\Service
 */
class MeasurementService extends AbstractService
{
    public function measureTopologyDiscoveryTime($sampleSize, $retryTimes = 10)
    {
        $measurementCollection = new Object\TimeMeasurementCollection();

        for($i = 1; $i <= $sampleSize; $i++) {

            $attempts = 0;
            do {
                try
                {
                    $stopWatch = new Stopwatch();
                    $stopWatch->start('topology discovery measurement');
                    $topology = $this->getServiceContainer()
                        ->get('hybnet.topology_service_2')
                        ->discoverTopology();
                    $event = $stopWatch->stop('topology discovery measurement');
                    dump('trying');
                    if ($topology) {
                        $measurement = (new Object\Measurement())
                            ->setValue($event->getDuration());

                        $measurementCollection->addMeasurement($measurement);
                        dump('measured');
                    }
                } catch (\Exception $e) {
                    $attempts++;
                    continue;
                }
                break;
            } while($attempts < $retryTimes);

        }

        return $measurementCollection;
    }
}