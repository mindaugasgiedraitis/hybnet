<?php

namespace MeasurementBundle\Object;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class TimeMeasurementCollection
 * @package MeasurementBundle\Object
 */
class TimeMeasurementCollection
{
    /**
     * @var ArrayCollection
     */
    private $measurements;

    /**
     * @return ArrayCollection
     */
    public function getMeasurements()
    {
        return $this->measurements;
    }

    /**
     * @param ArrayCollection $measurements
     * @return TimeMeasurementCollection
     */
    public function setMeasurements(ArrayCollection $measurements)
    {
        $this->measurements = $measurements;
        return $this;
    }

    /** Custom methods */

    /**
     * TimeMeasurement constructor.
     */
    function __construct()
    {
        $this->measurements = new ArrayCollection();
    }

    /**
     * @param Measurement $measurement
     * @return TimeMeasurementCollection
     */
    public function addMeasurement(Measurement $measurement)
    {
        $this->measurements->add($measurement);
        return $this;
    }

    /**
     * @param Measurement $measurement
     * @return TimeMeasurementCollection
     */
    public function removeMeasurement(Measurement $measurement)
    {
        $this->measurements->removeElement($measurement);
        return $this;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return count($this->getMeasurements());
    }

    /**
     * @return float|int
     */
    public function getMean()
    {
        $sum = 0;
        /** @var Measurement $measurement */
        foreach ($this->getMeasurements() as $measurement) {
            $sum += $measurement->getValue();
        }
        return $sum / $this->getCount();
    }

    /**
     * @return float
     */
    public function getUncertainty()
    {
        $sumOfSquareDifferences = 0;
        $mean = $this->getMean();
        $count = $this->getCount();
        foreach ($this->getMeasurements() as $measurement) {
            $sumOfSquareDifferences += pow($measurement->getValue() - $mean, 2);
        }
        return sqrt($sumOfSquareDifferences / ($count * ($count - 1)));
    }

    /**
     * @return array
     */
    public function toResultArray()
    {
        return array(
            'count' => $this->getCount(),
            'mean' => $this->getMean(),
            'uncertainty' => $this->getUncertainty(),
        );
    }
}