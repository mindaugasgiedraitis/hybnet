<?php

namespace MeasurementBundle\Object;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class Measurement
 * @package MeasurementBundle\Object
 */
class Measurement
{
    /**
     * @var float
     */
    private $value;

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return Measurement
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}