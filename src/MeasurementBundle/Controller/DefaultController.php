<?php

namespace MeasurementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class DefaultController
 * @package MeasurementBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function measureDiscoveryAjaxAction(Request $request)
    {
        ini_set('memory_limit','256M');
        set_time_limit(600);

        $sampleSize = $request->get('sample_size', 100) ? : 100;
        try {
            $result = $this->get('hybnet.measurement')->measureTopologyDiscoveryTime($sampleSize);
            $response = new Response(json_encode($result->toResultArray()));
        } catch (\Exception $e) {
            $response = new Response($e->getMessage());
        }

        return $response;
    }
}
