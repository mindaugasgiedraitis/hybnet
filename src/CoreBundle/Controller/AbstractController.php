<?php

namespace CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class AbstractController
 * @package CoreBundle\Controller
 */
abstract class AbstractController extends Controller
{

}
