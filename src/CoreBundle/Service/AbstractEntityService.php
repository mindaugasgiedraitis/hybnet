<?php

namespace CoreBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class AbstractEntityService
 * @package CoreBundle\Service
 */
abstract class AbstractEntityService extends AbstractService
{
    /**
     * @var EntityManagerInterface $entityManager
     */
    protected $entityManager;
    /**
     * @var EntityRepository $repository
     */
    protected $repository;

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager()
    {
        if (!$this->entityManager) {
            $this->entityManager = $this->getServiceContainer()->get('doctrine.orm.entity_manager');
        }
        return $this->entityManager;
    }

    /**
     * @param $entityManager
     * @return $this
     */
    public function setEntityManager(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param EntityRepository $repository
     * @return $this
     */
    public function setRepository(EntityRepository $repository)
    {
        $this->repository = $repository;
        return $this;
    }

    /* Custom methods */

    /**
     * @param int $id
     * @return object
     */
    public function find($id)
    {
        return $this->getEntityManager()
            ->getRepository($this->getRepository())
            ->find($id);
    }
}
