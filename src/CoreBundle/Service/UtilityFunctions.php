<?php

namespace CoreBundle\Service;



/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class UtilityFunctions
 * @package CoreBundle\Service
 */
class UtilityFunctions extends AbstractService
{
    /**
     * @param String $openFlowId
     * @return string
     */
    public function openFlowIdToChassisId($openFlowId)
    {
        $unsplitMac = substr(dechex($openFlowId), -12);
        $chunks = str_split($unsplitMac, 2);
        $chassisId = implode(':', $chunks);
        return $chassisId;
    }

    /**
     * @param array $snmpResult
     * @return array
     */
    public function flattenSNMPResultArray(array $snmpResult)
    {
        $flattenedArray = [];
        foreach ($snmpResult as $item) {
            $flattenedArray[$item['oid']] = $item['value'];
        }

        return $flattenedArray;
    }
}