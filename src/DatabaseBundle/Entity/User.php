<?php

namespace DatabaseBundle\Entity;

use CoreBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class User
 * @package DatabaseBundle\Entity
 *
 * @ORM\Entity(repositoryClass="DatabaseBundle\EntityRepository\UserRepository")
 * @ORM\Table(name="user",indexes={@ORM\Index(name="username_idx", columns={"username"})},
 * options={"collate"="utf8_lithuanian_ci"})
 */
class User extends AbstractEntity
{
    /**
     * @var string $username
     * @ORM\Column(name="username", type="string", length=128)
     */
    protected $username;

    /**
     * @var string $password
     * @ORM\Column(name="password", type="string", length=128, nullable=true)
     */
    protected $password;

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

}