<?php

namespace DatabaseBundle\Entity;

use CoreBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class ManagedObject
 * @package DatabaseBundle\Entity
 *
 * @ORM\Entity(repositoryClass="DatabaseBundle\EntityRepository\ManagedObjectRepository")
 * @ORM\Table(name="managed_object",indexes={@ORM\Index(name="oid_idx", columns={"oid"})},
 * options={"collate"="utf8_lithuanian_ci"})
 */
class ManagedObject extends AbstractEntity
{
    /**
     * @var string $OID
     * @ORM\Column(name="oid", type="string", length=255, nullable=true)
     */
    protected $OID;

    /**
     * @return string
     */
    public function getOID()
    {
        return $this->OID;
    }

    /**
     * @param string $OID
     * @return ManagedObject
     */
    public function setOID($OID)
    {
        $this->OID = $OID;
        return $this;
    }

}