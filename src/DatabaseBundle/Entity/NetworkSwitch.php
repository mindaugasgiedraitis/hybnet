<?php

namespace DatabaseBundle\Entity;

use CoreBundle\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class NetworkSwitch
 * @package DatabaseBundle\Entity
 *
 * @ORM\Entity(repositoryClass="DatabaseBundle\EntityRepository\NetworkSwitchRepository")
 * @ORM\Table(name="network_switch",indexes={@ORM\Index(name="ip_address_idx", columns={"ip_address"})},
 * options={"collate"="utf8_lithuanian_ci"})
 */
class NetworkSwitch extends AbstractEntity
{
    /**
     * @var string $ipAddress
     * @ORM\Column(name="ip_address", type="string", length=15, nullable=false)
     */
    protected $ipAddress;

    /**
     * @var string $name
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    protected $name;

    /**
     * @var bool
     * @ORM\Column(name="published", type="boolean")
     */
    protected $published = true;

    /**
     * @return string
     */
    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    /**
     * @param string $ipAddress
     * @return NetworkSwitch
     */
    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return NetworkSwitch
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param bool $published
     * @return NetworkSwitch
     */
    public function setPublished($published)
    {
        $this->published = $published;
        return $this;
    }


}
