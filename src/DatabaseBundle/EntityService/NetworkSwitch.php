<?php

namespace DatabaseBundle\EntityService;

use CoreBundle\Service\AbstractEntityService;
use Doctrine\ORM\NoResultException;
use DatabaseBundle\Entity;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class NetworkSwitch
 * @package DatabaseBundle\EntityService
 */
class NetworkSwitch extends AbstractEntityService
{
    /**
     * @var string $repository
     */
    protected $repository = 'DatabaseBundle:NetworkSwitch';

    /**
     * @return Entity\NetworkSwitch[]|null
     */
    public function getAllPublished()
    {
        $source = $this->getEntityManager()
            ->getRepository($this->getRepository())
            ->createQueryBuilder('ns')
            ->where('ns.published = :published')
            ->setParameter('published', true);

        try {
            return $source->getQuery()->getResult();
        } catch (NoResultException $e) {
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param string $name
     * @return Entity\NetworkSwitch|null
     */
    public function getByName($name)
    {
        $source = $this->getEntityManager()
            ->getRepository($this->getRepository())
            ->createQueryBuilder('ns')
            ->where('ns.name = :name')
            ->setParameter('name', $name);

        try {
            return $source->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param array $parameters
     * @return Entity\NetworkSwitch
     * @throws \Exception
     */
    public function save(array $parameters)
    {
        $name = (isset($parameters['name']) && $parameters['name']) ? $parameters['name'] : '';
        $address = (isset($parameters['address']) && $parameters['address']) ? $parameters['address'] : '';
        $published = isset($parameters['published']) ? $parameters['published'] : true;

        if (!$name || !$address) {
            throw new \Exception('Some parameters are missing');
        }

        if ($this->getByName($name)) {
            $switch = $this->getByName($name);
        } else {
            $switch = new Entity\NetworkSwitch();
        }

        $em = $this->getEntityManager();
        $switch->setName($name)
            ->setIpAddress($address)
            ->setPublished($published);
        $em->persist($switch);
        $em->flush();

        return $switch;
    }
}