<?php

namespace DatabaseBundle\EntityService;

use CoreBundle\Service\AbstractEntityService;
use DatabaseBundle\Entity;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class User
 * @package DatabaseBundle\EntityService
 */
class User extends AbstractEntityService
{
    /**
     * @var string $repository
     */
    protected $repository = 'DatabaseBundle:User';
}