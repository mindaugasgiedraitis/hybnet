<?php

namespace DatabaseBundle\EntityService;

use CoreBundle\Service\AbstractEntityService;
use DatabaseBundle\Entity;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class ManagedObject
 * @package DatabaseBundle\EntityService
 */
class ManagedObject extends AbstractEntityService
{
    /**
     * @var string $repository
     */
    protected $repository = 'DatabaseBundle:ManagedObject';
}