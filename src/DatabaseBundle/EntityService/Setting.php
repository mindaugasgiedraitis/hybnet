<?php

namespace DatabaseBundle\EntityService;

use CoreBundle\Service\AbstractEntityService;
use DatabaseBundle\Entity;
use Doctrine\ORM\NoResultException;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class Setting
 * @package DatabaseBundle\EntityService
 */
class Setting extends AbstractEntityService
{
    /**
     * @var string $repository
     */
    protected $repository = 'DatabaseBundle:Setting';

    /**
     * @param string $name
     * @return Entity\Setting|null
     */
    public function getByName($name)
    {
        $source = $this->getEntityManager()
            ->getRepository($this->getRepository())
            ->createQueryBuilder('s')
            ->where('s.name = :name')
            ->setParameter('name', $name);

        try {
            return $source->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param string $name
     * @return null|string
     */
    public function getValueByName($name)
    {
        try {
            return $this->getByName($name)->getValue();
        } catch (NoResultException $e) {
            return null;
        } catch (\Exception $e) {
            return null;
        }
    }

    /**
     * @param array $setting
     * @return Entity\Setting|null
     * @throws \Exception
     */
    public function save(array $setting)
    {
        if (!isset($setting['name']) || !$setting['name']) throw new \Exception('Setting name not set or empty');
        if (!isset($setting['type']) || !$setting['type']) throw new \Exception('Setting type not set or empty');
        if (!isset($setting['value'])) throw new \Exception('Setting value not set');

        $settingObject = $this->getByName($setting['name']);
        $em = $this->getEntityManager();

        if (!$settingObject) {
            $settingObject = new \DatabaseBundle\Entity\Setting();
            $settingObject->setName($setting['name']);
            $settingObject->setType($setting['type']);
            $settingObject->setValue($setting['value']);

            $em->persist($settingObject);
            $em->flush();
        } else {
            $settingObject->setName($setting['name']);
            $settingObject->setType($setting['type']);
            $settingObject->setValue($setting['value']);

            $em->flush();
        }

        return $settingObject;
    }
}