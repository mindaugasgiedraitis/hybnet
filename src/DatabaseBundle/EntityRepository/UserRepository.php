<?php

namespace DatabaseBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class UserRepository
 * @package DatabaseBundle\EntityRepository
 */
class UserRepository extends EntityRepository
{
    /*
     * Warning: do not put anything here, use EntityServices instead.
     */
}