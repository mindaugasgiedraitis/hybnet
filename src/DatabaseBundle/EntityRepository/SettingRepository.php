<?php

namespace DatabaseBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class SettingRepository
 * @package DatabaseBundle\EntityRepository
 */
class SettingRepository extends EntityRepository
{
    /*
     * Warning: do not put anything here, use EntityServices instead.
     */
}