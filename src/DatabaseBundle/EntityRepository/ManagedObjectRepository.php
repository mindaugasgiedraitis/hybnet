<?php

namespace DatabaseBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;

/**
 * @author Mindaugas Giedraitis <mindaugas.giedraitis7@gmail.com>
 *
 * Class ManagedObjectRepository
 * @package DatabaseBundle\EntityRepository
 */
class ManagedObjectRepository extends EntityRepository
{
    /*
     * Warning: do not put anything here, use EntityServices instead.
     */
}